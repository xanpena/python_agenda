# -*- coding: utf-8 -*-
import csv

class Contact:

    def __init__(self, name, email, phone):
        self.name = name
        self.phone = phone
        self.email = email


class ContactBook:

    def __init__(self):
        self._contacts = []

    def add(self, name, phone, email):
        contact = Contact(name, phone, email)
        self._contacts.append(contact)
        self._save()

    def show_all(self):
        for contact in self._contacts:
            self._print_contact(contact)

    def delete(self, name):
        for idx, contact in enumerate(self._contacts):
            if contact.name.lower() == name.lower():
                del self._contacts[idx]
                self._save()
                break

    def search(self, name):
        for contact in self._contacts:
            if contact.name.lower() == name.lower():
                self._print_contact(contact)
            else:
                self._not_found()

    def _save(self):
        with open('contacts.csv', 'w') as f:
            writer = csv.writer(f)
            writer.writerow(('name', 'phone', 'email'))

            for contact in self._contacts:
                writer.writerow((contact.name, contact.phone, contact.email))

    def _print_contact(self, contact):
        print(' --- * --- * --- * --- * ---')
        print('Nombre: {}'.format(contact.name))
        print('Teléfono: {}'.format(contact.phone))
        print('Email: {}'.format(contact.email))
        print(' --- * --- * --- * --- * ---')

    def _not_found(self):
        print('**********')
        print('Contacto no encontrado')
        print('**********')


def run():

    contact_book = ContactBook()

    with open('contacts.csv', 'r') as f:
        reader = csv.reader(f)
        for idx, row in enumerate(reader):
            if idx == 0:
                continue
            
            contact_book.add(row[0], row[1], row[2])
    

    while True:

        comando = str(raw_input('''
        ...:::CONTACTOS:::...
        -Teclea una opción-
        [a]ñadir contacto
        [ac]tualizar contacto
        [b]uscar contacto
        [e]liminar contacto
        [l]istar contactos
        [s]alir
        '''))

        print(comando)

        if comando == 'a':
            print('Añadir contacto')
            name = str(raw_input('Nombre: '))
            email = str(raw_input('Email: '))
            phone = str(raw_input('Teléfono: '))
            
            contact_book.add(name, phone, email)
        
        elif comando == 'ac':
            print('Actualizar contacto')

        elif comando == 'b':
            print('Buscar contacto')
            name = str(raw_input('Nombre: '))

            contact_book.search(name)

        elif comando == 'e':
            print('Eliminar contacto')
            name = str(raw_input('Nombre: '))

            contact_book.delete(name)

        elif comando == 'l':
            print('Listar contactos')
            contact_book.show_all()

        elif comando == 's':
            break
        else:
            print('Error de comando')

if __name__ == '__main__':
    run()